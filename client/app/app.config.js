(function () {
    angular
        .module("assessmentApp")
        .config(weddingGramAppConfig);
    weddingGramAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function weddingGramAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("SignIn", {
                url: "/signIn",
                views: {
                    "content": {
                        templateUrl: "../app/users/login.html"
                    }
                },
                controller: 'LoginCtrl',
                controllerAs: 'ctrl'
            })
            .state("SignUp", {
                url: "/signUp",
                views: {
                    "content": {
                        templateUrl: "../app/users/register.html"
                    }
                },
                controller: 'RegisterCtrl',
                controllerAs: 'ctrl'
            })
            .state('profile', {
                url: '/profile',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/profile.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'ProfileCtrl',
                controllerAs: 'ctrl'
            })
            .state('profile_list', {
                url: '/profile_list',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/profile_list.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'ProfileListCtrl',
                controllerAs: 'ctrl'
            })
            .state('edit_profile', {
                url: '/edit_profile/:profileId',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/edit_profile.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'EditProfileCtrl',
                controllerAs: 'ctrl'
            })            
            .state('aboutus', {
                url: '/aboutus',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/aboutus.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                }
            })

        $urlRouterProvider.otherwise("/signIn");


    }
})();
