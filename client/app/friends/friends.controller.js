(function(){

    angular
        .module("assessmentApp")
        .controller("FriendsCtrl", ["UserAPI", FriendsCtrl]);

    function FriendsCtrl(UserAPI){
        var vm = this;
        vm.profiles = [];
        UserAPI.getProfiles().then(function(result){
            console.log(result.data);
            vm.profiles = result.data;
        });
    }
})();
