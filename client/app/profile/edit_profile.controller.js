(function () {
    angular
        .module("assessmentApp")
        .controller("EditProfileCtrl", ["$sanitize", "$state", "AuthFactory", "Flash", "UserAPI", EditProfileCtrl]);

    function EditProfileCtrl($sanitize, $state, AuthFactory, Flash, UserAPI){
        var vm = this;
        vm.emailAddress = "";
        vm.firstName = "";
        vm.lastName = "";
        vm.password = "";
        vm.confirmpassword = "";

        init();
        function init(){
            UserAPI
                .retrieveProfile($state.params.profileId)
                .then(function(result){
                    //vm.emailAddress = result.data.email;
                    vm.firstName = result.data.firstName;
                    vm.lastName = result.data.lastName;
                      
                }).catch(function(error){
                    console.log(JSON.stringify(error));
                })
        }

        vm.update = function () {
            UserAPI
                .updateProfile($state.params.profileId, vm.firstName, vm.lastName)
                .then(function(response){
                    console.log(JSON.stringify(response.data));
                    $state.go('profile_list');
                }).catch(function(err){
                    console.log(err);
                });
        };

    }
})();