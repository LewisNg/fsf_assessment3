(function(){

    angular
        .module("assessmentApp")
        .controller("ProfileListCtrl", ["UserAPI", ProfileListCtrl]);

    function ProfileListCtrl(UserAPI){
        var vm = this;
        vm.profiles = [];
        vm.isAdmin = false;
        UserAPI.getProfiles().then(function(result){
            //console.log(result.data);
            vm.profiles = result.data;
            UserAPI.getLocalProfile().then(function(result){
                if (result.data.isAdmin == 1) {
                    vm.isAdmin = true;
                }
            })
        });
    }
})();
