(function () {
    angular.module("assessmentApp")
        .service("UserAPI", [
            "$http",
            "$q",
            UserAPI
        ]);

    function UserAPI($http, $q) {
        var self = this;

        self.getLocalProfile = function (callback){
            var defer = $q.defer();
            $http.get("/api/user/view-profile")
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };

        self.getProfiles = function (callback){
            var defer = $q.defer();
            $http.get("/api/user/profiles")
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };

        self.retrieveProfile = function (searchString){
            return $http({
                method: 'GET',
                url: "/api/profile",
                params: {'searchString': searchString}
            });
        }

        self.updateProfile = function (id, firstName, lastName){
            return $http({
                method: 'PUT',
                url: "/api/profile/" + id,
                data: {
                    firstName: firstName, 
                    lastName: lastName
                }
            });
        }
        // self.getAllSocialLoginsProfile = function (callback){
        //     var defer = $q.defer();
        //     $http.get("/api/user/social/profiles")
        //         .then(function(result){
        //             defer.resolve(result);
        //         }).catch(function(error){
        //         defer.reject(error);
        //     });
        //     return defer.promise;
        // };

        ///////////////////////////////////////////////////////
        // self.getLocalProfileToken = function (resetToken){
        //     var defer = $q.defer();
        //     $http.get("/api/user/get-profile-token?resetToken=" + resetToken)
        //         .then(function(result){
        //             defer.resolve(result);
        //         }).catch(function(error){
        //             defer.reject(error);
        //         });
        //     return defer.promise;
        // };

        // self.changePasswordToken = function(data) {
        //     return $http({
        //         method: 'POST',
        //         url: '/api/user/change-passwordToken',
        //         data: data
        //     }).then(function (result) {
        //         return result.data;
        //     });    
        // }



    }
})();