(function () {
    angular
        .module("assessmentApp", [
            "ngFileUpload",
            "ui.router",
            "ngFlash",
            "ngSanitize",
            "ngProgress",
            "ngMessages",
            "data-table"
        ]);
})();