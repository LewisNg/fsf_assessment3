var Sequelize = require('sequelize');
var config = require("./config");

console.log(config.mysql);
var database = new Sequelize(config.mysql, {
    pool: {
        max: 2,
        min: 1,
        idle: 10000
    }
});

var User = require("./models/user.model.js")(database);
var Friends = require("./models/friends.model.js")(database);

// var AuthProvider = require("./models/authentication.provider.model.js")(database);

// BEGIN: MYSQL RELATIONS

User.hasMany(Friends, {foreignKey: 'user1_email'});
User.hasMany(Friends, {foreignKey: 'user2_email'});
// Friends.belongsTo(User);
// END: MYSQL RELATIONS
//{force: config.seed}
database
    .sync({force: config.seed})
    .then(function () {
        console.log("Database in Sync Now");
        require("./seed")();
    });

module.exports = {
    User: User,
    Friends: Friends
    // Post: Post,
    // Comment: Comment,
    // AuthProvider: AuthProvider,
};

