var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('user', {
        username: {
            type : Sequelize.STRING,
            allowNull: false
        },
        password: {
            type: Sequelize.STRING,
            allowNull: true
        },
        firstName: Sequelize.STRING,
        lastName: Sequelize.STRING,
        email: {
            type:Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        addressLine1: Sequelize.STRING,
        addressLine2: Sequelize.STRING,
        city: Sequelize.STRING,
        postcode: Sequelize.STRING,
        phone: Sequelize.STRING,
        isAdmin: {
            type: Sequelize.BOOLEAN,
            allowNull: true
        },
        deleted_at: {
            type: Sequelize.DATE,
            allowNull: true
        }
    });
};