var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('friend', {
        user1_email: {
            type : Sequelize.STRING,
            allowNull: false,
            references: {
                model: 'users',
                key: 'email'
            }
        },
        user2_email: {
            type: Sequelize.STRING,
            allowNull: false,
            allowNull: false,
            references: {
                model: 'users',
                key: 'email'
            }
        },
        status: {
            type: Sequelize.STRING,
            allowNull: false
        }
    },
    {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at'
    });
};