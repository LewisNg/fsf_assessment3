'use strict';
var domain_name = "http://localhost:3000";

module.exports = {
    mysql: "mysql://root:mySQ1!sql@localhost/assessment3?reconnect=true",
    domain_name: domain_name,
    port: 3000,
    seed: true
};
