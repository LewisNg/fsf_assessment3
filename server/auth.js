var LocalStrategy = require("passport-local").Strategy;
// var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
// var FacebookStrategy = require("passport-facebook").Strategy
// var LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
// var WechatStrategy = require("passport-wechat").Strategy
// var TwitterStrategy  = require('passport-twitter').Strategy;
var bcrypt   = require('bcryptjs');

var User = require("./database").User;
//var AuthProvider = require("./database").AuthProvider;
var config = require("./config");

//Setup local strategy
module.exports = function (app, passport) {
    function authenticate(username, password, done) {

        User.findOne({
            where: {
                email: username
            }
        }).then(function(result) {
            if(!result){
                return done(null, false);
            }else{
                if(bcrypt.compareSync(password , result.password)){
                    var whereClause = {};
                    whereClause.email = username;
                    User
                    .update({ reset_password_token: null},
                    {where:  whereClause});
                    return done(null, result);
                }else{
                    return done(null, false);
                }
            }
        }).catch(function(err){
            return done(err, false);
        });


    }

    passport.use(new LocalStrategy({
        usernameField: "email",
        passwordField: "password"
    }, authenticate));

    passport.serializeUser(function (user, done) {
        console.info("serial to session");
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        User.findOne({
            where: {
                email: user.email
            }
        }).then(function(result) {
            if(result){
                done(null, user);
            }
        }).catch(function(err){
            done(err, user);
        });
    });

};
